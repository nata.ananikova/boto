import boto3
from operator import itemgetter


ec2_client = boto3.client('ec2', region_name="eu-west-3")


volumes = ec2_client.describe_snapshots(
    Filters=[
        {
            'Name': 'tag:Name',
            'Values': ['prod', 'staging']
        }
    ]
)

for volume in volumes:
    snapshots = ec2_client.describe_snapshots(
        OwnerIds=['self'],
        # Filters=[
        #     {
        #         'Name': 'volume-id',
        #         'Values': [volume[1]]
        #     }
        # ]
    )

    sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)

    # for snap in snapshots['Snapshots']:
    #     print(snap['StartTime'])
    #
    # print('#############')
    #
    # for snap in sorted_by_date:
    #     print(snap['StartTime'])

    for snap in sorted_by_date:
        response = ec2_client.delete_snapshot(
            SnapshotId=snap['SnapshotId']
        )
        print(response)