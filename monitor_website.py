import paramiko as paramiko
import requests
import smtplib
import os

EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')

response = requests.get('http://hostname:port')

if response.status_code == 200:
    print('App is running successfully!')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='', username='ec2-user', key_filename='/Users/.ssh/vm_key.pem')
    stdin, stdout, stderr = ssh.exec_command('docker ps')
    print('\n'.join(stdout.readlines()))
    ssh.close()

#     print('App is down. Fix it.')
#     with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
#         smtp.starttls()
#         smtp.ehlo()
#         smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
#         msg = "Subject: SITE DOWN\nFix the issue!"
#         smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)




